<?php
// $Id$


/**
 * SoftLayer hardware page.
 */
function softlayer_hardware_page() {
  $output = '';
  
  // TODO API calls - http://sldn.softlayer.com/wiki/index.php/SoftLayer_Hardware
  //getActiveComponents
  //getAttributes
  //getComponents
  
  // Temp array (for dev).
  // TODO Delete
  $hardware = array(
    array(
		    'id'                       => 123,
		    'hardwareId'               => 1234567890,
		    'hardwareComponentModelId' => 1111111111,
		    'modifyDate'               => '2011-01-01T00:00:00-07:00',
		    'name'                     => 'SL Server',
		    'serialNumber'             => '100000000000000000000000A',
    ),
    array(
		    'id'                       => 456,
		    'hardwareId'               => 2345678901,
		    'hardwareComponentModelId' => 1111111111,
		    'modifyDate'               => '2011-02-01T00:00:00-07:00',
		    'name'                     => 'SL Server',
		    'serialNumber'             => '100000000000000000000000A',
    ),
    array(
		    'id'                       => 789,
		    'hardwareId'               => 3456789012,
		    'hardwareComponentModelId' => 1111111111,
		    'modifyDate'               => '2011-01-03T00:00:00-07:00',
		    'name'                     => 'SL Server Lite',
		    'serialNumber'             => '100000000000000000000000L',
    ),
  );
  
  $output .= theme('table', array_keys($hardware[0]), $hardware, NULL, t('Hardware'));
  
  return $output;
}



/**
 * SoftLayer tickets page.
 */
function softlayer_tickets_page() {
  $output = '';

  // Temp array (for dev).
  // TODO Delete
  $tickets = array(
    array(
		    'id'               => 123,
		    'title'            => 'Super powerful settings update needed',
		    'accountId'        => 1234567890,
		    'assignedUserId'   => 1111111111,
		    'createDate'       => 'SL Server',
		    'createDate'       => '2011-01-01T00:00:00-07:00',
		    'finalComments'    => 'Thanks for your help with all th....',
		    'groupId'          => 'SUPPORT',
		    'locationId'       => 'LAX-1-1-1',
		    'modifyDate'       => '2011-01-01T02:00:00-07:00',
		    'userEditableFlag' => TRUE,
    ),
    array(
		    'id'               => 123,
		    'title'            => 'Super powerful settings update needed',
		    'accountId'        => 1234567890,
		    'assignedUserId'   => 1111111111,
		    'createDate'       => 'SL Server',
		    'createDate'       => '2011-01-02T00:00:00-07:00',
		    'finalComments'    => 'Thanks for your help with all th....',
		    'groupId'          => 'SUPPORT',
		    'locationId'       => 'LAX-1-1-1',
		    'modifyDate'       => '2011-01-02T02:00:00-07:00',
		    'userEditableFlag' => TRUE,
    ),
    array(
		    'id'               => 123,
		    'title'            => 'Super powerful settings update needed',
		    'accountId'        => 1234567890,
		    'assignedUserId'   => 1111111111,
		    'createDate'       => 'SL Server',
		    'createDate'       => '2011-01-03T00:00:00-07:00',
		    'finalComments'    => 'Thanks for your help with all th....',
		    'groupId'          => 'SUPPORT',
		    'locationId'       => 'LAX-1-1-1',
		    'modifyDate'       => '2011-01-03T02:00:00-07:00',
		    'userEditableFlag' => TRUE,
    ),
  );
  
  $output .= theme('table', array_keys($tickets[0]), $tickets, NULL, t('Open Tickets'));

  // Temp array (for dev).
  // TODO Delete
  $tickets = array(
    array(
		    'id'               => 123,
		    'title'            => 'Super powerful settings update needed',
		    'accountId'        => 1234567890,
		    'assignedUserId'   => 1111111111,
		    'createDate'       => 'SL Server',
		    'createDate'       => '2011-01-01T00:00:00-07:00',
		    'finalComments'    => 'Thanks for your help with all th....',
		    'groupId'          => 'SUPPORT',
		    'locationId'       => 'LAX-1-1-1',
		    'modifyDate'       => '2011-01-01T02:00:00-07:00',
		    'userEditableFlag' => TRUE,
    ),
  );
  
  $output .= theme('table', array_keys($tickets[0]), $tickets, NULL, t('Latest Closed Tickets'));
  
  return $output;
}
