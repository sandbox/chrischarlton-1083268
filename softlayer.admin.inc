<?php
// $Id$


/**
 * API Settings page.
 */
function softlayer_api_settings_page() {
  $output = '';
  // Show other options if API key is saved.
  if (softlayer_api_key_check()) {
    // Refresh rate.
    $output .= drupal_get_form('softlayer_api_refresh_rate_form');
  }
  // API key.
  $output .= drupal_get_form('softlayer_api_key_form');
  return $output;
}


function softlayer_api_settings_page2() {
	return 'POW!';
}




/**
 * API key form.
 * @param Array $form_state
 */
function softlayer_api_key_form($form_state) {
  $form = array();
  
  $form['api'] = array(
    '#type' => 'fieldset', 
    '#title' => t('API key'), 
    '#weight' => 90, 
    '#collapsible' => TRUE, 
    '#collapsed' => softlayer_api_key_check(),
  );
  
  // API username
  $form['api']['api_username'] = array(
    '#type' => 'textfield', 
    '#title' => t('API Username'), 
    '#description' => t('Enter the API username.'), 
    '#default_value' => variable_get('softlayer_api_username', NULL), 
    '#size' => 12, 
    '#maxlength' => 20, 
    '#weight' => 1,
    '#required' => !softlayer_api_key_check(),
    '#disabled' => softlayer_api_key_check(),
  );
  // API username
  $form['api']['api_key'] = array(
    '#type' => 'textfield', 
    '#title' => t('API Key'), 
    '#description' => t('Enter the API key provided by SoftLayer.'), 
    '#default_value' => variable_get('softlayer_api_key', NULL), 
    '#size' => 40, 
    '#maxlength' => 128, 
    '#weight' => 2,
    '#required' => !softlayer_api_key_check(),
    '#disabled' => softlayer_api_key_check(),
  );

  // Submit button.
  if (!softlayer_api_key_check()) {
		  $form['api']['submit'] = array(
		    '#type'     => 'submit',
		  		'#value'    => t('Save settings'),
		    '#validate' => array('softlayer_api_key_form_validate'),
		    '#submit'   => array('softlayer_api_key_form_submit'),
		    '#weight'   => 10,
		  );
  }
  else {
		  $form['api']['clear'] = array(
		    '#type'     => 'submit',
		  		'#value'    => t('Clear API key'),
		    '#validate' => array(),
		    '#submit'   => array('softlayer_api_key_form_clear_submit'),
		    '#weight'   => 10,
		  );
  }
    
  return $form;
}


/**
 * Form validation.
 * @param String $form
 * @param Array $form_state
 */
function softlayer_api_key_form_validate($form, &$form_state) {
  $form_values = $form_state['values'];
  
  // Username validation.
  $min_username_length = 4;
  if (strlen($form_values['api_username']) < $min_username_length) {
    form_set_error('api_username', t('API username must be at least @min_char characters long.', array('@min_char' => $min_username_length)));
  }
  
  // API key validation.
  $min_api_key_length = 10;
  if (strlen($form_values['api_key']) < $min_api_key_length) {
    form_set_error('api_key', t('API key is too short. Please check to make sure your key was entered correctly.'));
  }
}

/**
 * Form submit handler.
 * @param Array $form
 * @param Array $form_state
 */
function softlayer_api_key_form_submit($form, &$form_state) {
  $form_values = $form_state['values'];
  variable_set('softlayer_api_username', $form_values['api_username']);
  variable_set('softlayer_api_key', $form_values['api_key']);
}




/**
 * Form submit to clear API key values.
 * @param Array $form
 * @param Array $form_state
 */
function softlayer_api_key_form_clear_submit($form, &$form_state) {
  variable_set('softlayer_api_username', NULL);
  variable_set('softlayer_api_key', NULL);
}



/**
 * Checks if the API key has been saved or not.
 * @return BOOL
 */
function softlayer_api_key_check() {
  $api_username = variable_get('softlayer_api_username', FALSE);
  $api_key = variable_get('softlayer_api_key', FALSE);
  if (!empty($api_username) && !empty($api_key)) {
    return TRUE;
  }
  return FALSE;
}




/**
 * Form builder for API data `refresh` rate.
 * @param Array $form_state
 */
function softlayer_api_refresh_rate_form($form_state) {
  $form = array();
  
  $form['refresh_rate'] = array(
    '#type' => 'select', 
    '#title' => t('Update data'), 
    '#default_value' => variable_get('softlayer_api_refresh_rate','day'),
    '#options' => array(
        'hour' => t('Hourly'), 
        'day' => t('Daily'), 
      ),
   '#description' => t('How often do you want the SoftLayer site data to be updated.'),
  );
  
  return $form;
}


/**
 * Form validation.
 * @param String $form
 * @param Array $form_state
 */
function softlayer_api_refresh_rate_form_validate($form, &$form_state) {
}

/**
 * Form submit handler.
 * @param String $form
 * @param Array $form_state
 */
function softlayer_api_refresh_rate_form_submit($form, &$form_state) {
  $form_values = $form_state['values'];
  variable_set('softlayer_api_refresh_rate', $form_values['refresh_rate']);
}
